let mail = document.getElementById('mail')
let pass = document.getElementById('pass')
let role = document.getElementsByName('Role')
let gender = document.getElementById('gender')
let permissions = document.getElementsByClassName('permission')
let btnSubmit = document.getElementById('btnSubmit')
let inpForm = document.getElementById('inpForm')
let errMsg = document.getElementById('errorMsg')
let border = document.getElementById('border')
let confirm = document.getElementsByClassName('confirm')


let checkPermissions = function (per) {
    let count = 0;
    for (let i of per) {
        if (i.checked) {
            count++;
        }
    }
    console.log(count)
    return count;
}
let checkedRadio = function (role) {
    let c = false;
    for (let i of role) {
        if (i.checked) {
            c = true;
        }
    }
    console.log(c)
    return c;
}


inpForm.addEventListener('submit', (e) => {
    errorMsg.innerText = ''
    if (mail.value === '' || pass.value === '' || checkedRadio(role) === false || checkPermissions(permissions) === 0) {
        errorMsg.innerText = "Please fill all the details first!"
    }
    else {
        let pas = pass.value
        if (!(pas.match(/^(?=.*[a-z])(?=.*[0-9])(?=.*[A-Z]).{6,}$/)) || (checkPermissions(permissions) < 2)) {
            errorMsg.innerText = "password should be min 6 character with MIX of Uppercase, lowercase, digits \n must check atleast 2 permissions"
        }

        else {


            for (let v of confirm) {
                v.innerText = "✅"
            }
            /* it.innerText= */
            mail.setAttribute('disabled', 'disabled')


            pass.setAttribute('disabled', 'disabled')


            for (let x of role) {
                x.setAttribute('disabled', 'disabled')

            }
            for (let y of permissions) {
                y.setAttribute('disabled', 'disabled')

            }
            gender.setAttribute('disabled', 'disabled')

            btnSubmit.remove();
        }
    }

    e.preventDefault();
})
